/*
Requires Underscore.js
*/


var centroids = [[23, 230, 18],   // Green
                 [18, 62, 181],   // Blue
                 [251, 18, 18],   // Red
                 [255, 255, 0],   // Yellow
                 [255, 152, 0]    // Orange
                ],
    k = centroids.length;

var Clusterer = function() {

    var data,

    init = function(_data) {
        data = _data;
    },

    euclid_dist = function(p1, p2) {
        var dx = p1[0] - p2[0],
            dy = p1[1] - p2[1],
            dz = p1[2] - p2[2],
            D = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2) + Math.pow(dz, 2));
        return D;
    },

    get_sorted_keys = function(values) {
        var keys_idx = [], i;
        for (i = 0; i < values.length; i++) {
            keys_idx.push(i);
        }

        var keys = _.sortBy(keys_idx, function(idx){ return values[idx]; });
        keys.reverse();
        return keys;
    },

    get_palette = function() {
        // Returns [ordered_rgb_palette, norm_factor, normed_scores]

        var THRESH = 180;
        var scores = [], i = k;
        while (i--) {  // Zero filling
            scores[i] = 0;
        }

        for (i = data.length - 1; i >= 0; --i) {
            var current = data[i];
            // Skip whites
            if (current[0] > 250 && current[1] > 250 && current[2] > 250)
                continue;

            var dist_partial = _.partial(euclid_dist, current),
                distances = _.map(
                                _.map(centroids, dist_partial),
                                Math.floor
                            ),
                min_dist = _.min(distances);

            // if(i % 4000 === 0){
            //     console.log("current", current);
            //     console.log("Dsts", distances);
            // }

            if (min_dist < THRESH){
                var argmin = _.indexOf(distances, min_dist);
                scores[argmin]++;
            }
        }
        var clusters_ranking = get_sorted_keys(scores),
            palette = _.map(clusters_ranking,
                            function(idx){ return centroids[idx]; }),
            max_score = _.max(scores),
            norm_scores = _.map(scores,
                            function(x){ return x / max_score; });

        return [palette, max_score, norm_scores.sort().reverse()];
    };

    return {
        // These are exposed (public) functions
        init          : init,
        get_palette   : get_palette
    };
};


var imagedataToRGB = function(_image_data) {
    // Translates an ImageData object's CanvasPixelArray into
    // an RGB array of the form:
    //
    //      [
    //          [ r1, g1, b1 ],
    //          [ r2, g2, b2 ],
    //          ...
    //          [ rN, gN, bN ]
    //      ]

    var rgb_array = [];
    var rgb_color;

    for (var i = _image_data.data.length - 1; i > 1; i -= 4) {
        rgb_color = [
            _image_data.data[i - 3],
            _image_data.data[i - 2],
            _image_data.data[i - 1]
        ];

        rgb_array.push(rgb_color);
    }

    return rgb_array;
};
