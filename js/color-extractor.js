/*
 * Released under the MIT license
 */


(function() {
    var cameraCanvas = document.getElementById("cam");
    var capturing = false;
    var overlay = new Image();
    overlay.src = "../images/overlay.png";


    var rgbToHex = function(r, g, b) {
        if(r < 0 || r > 255) alert("r is out of bounds; "+r);
        if(g < 0 || g > 255) alert("g is out of bounds; "+g);
        if(b < 0 || b > 255) alert("b is out of bounds; "+b);
        return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1,7);
    };

    var mirrorCanvas = function(canvas){
        var ctx = canvas.getContext("2d");
        ctx.translate(canvas.width, 0);
        ctx.scale(-1, 1);
    };

    var drawOverlay = function(canvas, overlay){
        var ctx = canvas.getContext("2d");
        mirrorCanvas(cameraCanvas);
        ctx.drawImage(overlay, 0, 0);
        mirrorCanvas(cameraCanvas);
    };

    var displayColors = function(colors) {
        // Display colors
        var hexColor1 = rgbToHex(colors[0][0], colors[0][1], colors[0][2]);
        var hexColor2 = rgbToHex(colors[1][0], colors[1][1], colors[1][2]);
        var hexColor3 = rgbToHex(colors[2][0], colors[2][1], colors[2][2]);
        var hexColor4 = rgbToHex(colors[3][0], colors[3][1], colors[3][2]);
        $('#box1').css('background-color', hexColor1);
        $('#box2').css('background-color', hexColor2);
        $('#box3').css('background-color', hexColor3);
        $('#box4').css('background-color', hexColor4);
    };

    camera.init({
        fps: 30,
        mirror: true,
        targetCanvas: cameraCanvas,

        onFrame: function(canvas) {
            var ctx = cameraCanvas.getContext("2d");

            drawOverlay(cameraCanvas, overlay);
        },

        onSuccess: function() {
            var shootBtn = document.getElementById("shoot");
            var sendBtn = document.getElementById("send");
            var retryBtn = document.getElementById("retry");

            document.getElementById("info").style.display = "none";

            capturing = true;
            document.getElementById("buttons").style.display = "block";
            shootBtn.onclick = function() {
                shootBtn.style.display = "none";
                sendBtn.style.display = "inline";
                retryBtn.style.display = "inline";

                camera.pause();
                capturing = !capturing;
            };

            retryBtn.onclick = function() {
                shootBtn.style.display = "block";
                sendBtn.style.display = "none";
                retryBtn.style.display = "none";

                camera.start();
                capturing = !capturing;
            };

            sendBtn.onclick = function() {
                sendBtn.disabled = true;
                retryBtn.disabled = true;

                // Fully blind the spare part of the picture
                drawOverlay(cameraCanvas, overlay);
                drawOverlay(cameraCanvas, overlay);
                drawOverlay(cameraCanvas, overlay);
                drawOverlay(cameraCanvas, overlay);

                // TODO: Show some 'waiting' animation

                // Send data to server
                var ctx = cameraCanvas.getContext("2d");
                var data = ctx.getImageData(0, 0, cameraCanvas.width, cameraCanvas.height);
                var rgbdata = imagedataToRGB(data);

                clusterer = Clusterer();
                clusterer.init(rgbdata);
                var palette = clusterer.get_palette();
                console.log("palette:", palette);

                displayColors(palette[0]);
            };
        },

        onError: function(error) {
            // TODO: log error
        },

        onNotSupported: function() {
            document.getElementById("info").style.display = "none";
            cameraCanvas.style.display = "none";
            document.getElementById("notSupported").style.display = "block";
        }
    });
})();